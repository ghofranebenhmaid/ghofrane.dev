import React from 'react';
import Title from '../components/Title/Title';
import photoprofile from '../assets/images/ghofranebenhmaid.jpg';

const About = () => {
   return (
      <div className=' about container'>
         <div className='about__title'>
            <div className='text--mb--s'>
               <Title headingStyle='subtitle' headingSize='heading--h4'>
                  about me
               </Title>
            </div>
            <div className='text--mb--m'>
               <Title headingStyle='bolder--800' headingSize='heading--h1'>
                  A passionate web developer with flair for user interface
                  design
               </Title>
            </div>

            <div className='aboute__descreption'>
               <Title headingStyle='normal--400' headingSize='heading--h4'>
                  I have always had a strong interest in photography and graphic
                  design. So beside studying IT & Multimedia, where I learned to
                  work with many programming languages, I also gained a lot of
                  practical experience in web design, graphic design,
                  photography and video editing by working different jobs in
                  Tunisia.
                  <br />
                  <br />
                  In Denmark, I have beside learning Danish and English,
                  completed the HackYourFuture web development educational
                  program, where I have gained more practical experience in both
                  frontend and backend development, and discovered my flair for
                  user interface design.
                  <br />
                  <br />
                  In my spare time, I have always used many hours on honing my
                  skills by making prototype webpages, and also designing logos
                  and doing video editing as a volunteer for different causes,
                  as well as practicing photography of both nature and city
                  sights.
               </Title>
            </div>
         </div>

         <img src={photoprofile} alt='photo profile' />

         <div className='skils flex--wrap'>
            <div className='skils__section text--mb--l'>
               <div className='text--mb--s'>
                  <Title headingStyle='subtitle' headingSize='heading--h4'>
                     Design
                  </Title>
               </div>

               <Title headingStyle='bolder--800' headingSize='heading--h2'>
                  <div className='skils__text flex--wrap'>
                     Art direction
                     <small>&#8725;</small>
                     Web & mobile
                     <small>&#8725;</small>
                     UX & UI
                     <small>&#8725;</small>
                     Typography
                     <small>&#8725;</small>
                     Animation
                     <small>&#8725;</small>
                     Photography
                  </div>
               </Title>
            </div>
            <div className='skils__section '>
               <div className='text--mb--s'>
                  <Title headingStyle='subtitle' headingSize='heading--h4'>
                     Technologies
                  </Title>
               </div>
               <Title headingStyle='bolder--800' headingSize='heading--h2'>
                  <div className='skils__text flex--wrap'>
                     React
                     <small>&#8725;</small>
                     Javascript
                     <small>&#8725;</small>
                     Node.js
                     <small>&#8725;</small>
                     Express
                     <small>&#8725;</small>
                     Wordpress
                     <small>&#8725;</small>
                     PHP
                     <small>&#8725;</small>
                     Html5
                     <small>&#8725;</small>
                     CSS3
                     <small>&#8725;</small>
                     Sass
                  </div>
               </Title>
            </div>
         </div>
      </div>
   );
};

export default About;
