import React, { useRef, useEffect } from 'react';

import bgTypography from '../assets/images/bg-typography.png';
import bgWeb from '../assets/images/web.jpg';
import logo from '../assets/images/logo.jpg';
import photography from '../assets/images/photograph.jpg';
import photog from '../assets/images/photog.jpg';

import Button from '../components/Button/Button';
import { Link } from 'react-router-dom';

const Home2 = () => {
   useEffect(() => {
      let slider = document.querySelector('.slider');
      let innerSlider = document.querySelector('.slider-inner');
      let pressed = true;
      let startx;
      let x;

      slider.addEventListener('mousedown', (e) => {
         pressed = true;
         startx = e.offsetX - innerSlider.offsetLeft;
         slider.style.cursor = 'grabbing';
      });
      slider.addEventListener('mouseenter', () => {
         slider.style.cursor = 'grab';
      });
      slider.addEventListener('mouseup', () => {
         slider.style.cursor = 'grab';
      });
      window.addEventListener('mouseup', () => {
         pressed = false;
      });

      slider.addEventListener('mousemove', (e) => {
         if (!pressed) return;
         e.preventDefault();

         x = e.offsetX;
         innerSlider.style.left = `${x - startx}px`;

         checkboundary();
      });

      function checkboundary() {
         let inner = slider.getBoundingClientRect();
         let outer = innerSlider.getBoundingClientRect();

         if (parseInt(innerSlider.style.left) > 0) {
            innerSlider.style.left = '0px';
         } else if (inner.right > outer.right) {
            innerSlider.style.left = `${inner.width - outer.width}px`;
         }
      }
   }, []);

   return (
      <div>
         <section className='home  container'>
            <div className='home__text'>
               <h1>Frontend developer</h1>
               <h2>Ghofrane</h2>
               <p>
                  I’m a skilled and diligent frontend developer with a passion
                  for React and a flair for user interface design. I have
                  experience with different programming languages and
                  technologies including HTML, CSS, JavaScript, Node.js,
                  Express.js, React, MySQL, Knex.js and Git as well as
                  WordPress. I’m also very skilled in using Adobe design
                  programs, such as Photoshop, Illustrator, InDesign, Lightroom,
                  Premier Pro, After Effects and 3Ds max. I’m educated in IT &
                  Multimedia, have completed the HackYourFuture web-development
                  education program, and have achieved a lot of
                  learning-by-doing through the years. I have a broad skillset
                  beside web-development including graphic design, user
                  interface design, typography and photo and video editing. In
                  my spare time I also enjoy photography and exploring the city
                  and the nature.
               </p>
               <div style={{ marginTop: '20px' }}>
                  <Button buttonSize='btn--small'>See Resume</Button>
               </div>
            </div>
            <div className='home__slider'>
               <div className='slider'>
                  <div className='slider-inner'>
                     <Link to='/logos'>
                        <div className='home__slider--image '>
                           <img src={logo} alt='logo' />

                           <a href='#'> Logo</a>
                        </div>
                     </Link>
                     <Link to='/photography'>
                        <div className='home__slider--image '>
                           <img src={photog} alt='Photography' />
                           <a href='#'> Photography</a>
                        </div>{' '}
                     </Link>
                     <Link to='/coding'>
                        <div className='home__slider--image'>
                           <img src={bgWeb} alt='website' />
                           <a href='#'> Website</a>
                        </div>{' '}
                     </Link>

                     <Link to='/typography'>
                        <div className='home__slider--image '>
                           <img src={bgTypography} alt='typography' />
                           <a href='#'> Typography</a>
                        </div>
                     </Link>
                  </div>
               </div>
            </div>
         </section>
      </div>
   );
};

export default Home2;
