import React from 'react';
import ContactForm from '../components/ContactForm/ContactForm.component';

const Contact = () => {
   return (
      <section className='container'>
         <ContactForm />
      </section>
   );
};

export default Contact;
