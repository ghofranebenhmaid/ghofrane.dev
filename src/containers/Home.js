import React from 'react'
import Hero from '../components/Hero/Hero.component'
import Section from '../components/Sections/Section.component'
import SectionCoding from '../components/Sections/SectionCoding.component'
import SectionTypographie from '../components/Sections/SectionTypographie.component'
import SectionLogos from '../components/Sections/SectionLogos.component'
import Profile from '../components/Profile/Profile.component'
import SkillsSection from '../components/SkillsSection'
import Title from '../components/Title/Title'

const Home = () => {
    return (
        <div >
            <Hero /> 
            <Profile/>
            <SectionCoding/>
            <SectionLogos/>
            <Section/>
            <SectionTypographie />
            <SkillsSection/>
           
        </div>
    )
}

export default Home
