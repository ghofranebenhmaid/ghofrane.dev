import React from 'react'
import Repositories from '../components/Repositories/Repositories.component'

const Portfolio = () => {
    return (
        <div>
            <Repositories />
        </div>
    )
}

export default Portfolio
