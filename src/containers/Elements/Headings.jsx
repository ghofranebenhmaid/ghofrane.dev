import React from 'react'
import Title from '../../components/Title/Title'

const Headings = () => {
    return (
        <div className='container'>
            <header className='text--mb--l'>
                <Title headingStyle='bolder--800' headingSize='heading--h1' > Headings</Title>
            </header>


            <section className='text--mb--m'> 
                <div className='text--mb--s'>
                <Title headingStyle='bolder--800' headingSize='heading--h1' > Heading 1</Title>
                </div>
            <Title headingStyle ='normal--400' headingSize='heading--small'>Lorem ipsum dolor sit amet, an vel iudico doctus comprehem, has an blandit moder atius liberavisse, vim dicta aeque choro ut. Debet simulus principes qui ut, melius necessi tat ibus an. Te posse idque disse ntiunt est, ne nec vidisse deleniti hendrerit. Legere grae per ut, ad eam altera electram. Sit volutpat dissentiet ex, eos soluta.</Title>
            </section>
            <section className='text--mb--m'> 
                <div className='text--mb--s'>
                <Title headingStyle='bolder--800' headingSize='heading--h2'> Heading 2</Title>
                </div>
            <Title headingStyle ='normal--400' headingSize='heading--small'>Lorem ipsum dolor sit amet, an vel iudico doctus comprehem, has an blandit moder atius liberavisse, vim dicta aeque choro ut. Debet simulus principes qui ut, melius necessi tat ibus an. Te posse idque disse ntiunt est, ne nec vidisse deleniti hendrerit. Legere grae per ut, ad eam altera electram. Sit volutpat dissentiet ex, eos soluta.</Title>
            </section>

            <section className='text--mb--m'> 
                <div className='text--mb--s'>
                <Title headingStyle='bolder--800' headingSize='heading--h3' > Heading 3</Title>
                </div>
            <Title headingStyle ='normal--400' headingSize='heading--small'>Lorem ipsum dolor sit amet, an vel iudico doctus comprehem, has an blandit moder atius liberavisse, vim dicta aeque choro ut. Debet simulus principes qui ut, melius necessi tat ibus an. Te posse idque disse ntiunt est, ne nec vidisse deleniti hendrerit. Legere grae per ut, ad eam altera electram. Sit volutpat dissentiet ex, eos soluta.</Title>
            </section>
            <section className='text--mb--m'> 
                <div className='text--mb--s'>
                <Title headingStyle='bolder--800' headingSize='heading--h4' > Heading 4</Title>
                </div>
            <Title headingStyle ='normal--400' headingSize='heading--small'>Lorem ipsum dolor sit amet, an vel iudico doctus comprehem, has an blandit moder atius liberavisse, vim dicta aeque choro ut. Debet simulus principes qui ut, melius necessi tat ibus an. Te posse idque disse ntiunt est, ne nec vidisse deleniti hendrerit. Legere grae per ut, ad eam altera electram. Sit volutpat dissentiet ex, eos soluta.</Title>
            </section>
            <section className='text--mb--m'> 
                <div className='text--mb--s'>
                <Title headingStyle='bolder--800' headingSize='heading--h5' > Heading 5</Title>
                </div>
            <Title headingStyle ='normal--400' headingSize='heading--small'>Lorem ipsum dolor sit amet, an vel iudico doctus comprehem, has an blandit moder atius liberavisse, vim dicta aeque choro ut. Debet simulus principes qui ut, melius necessi tat ibus an. Te posse idque disse ntiunt est, ne nec vidisse deleniti hendrerit. Legere grae per ut, ad eam altera electram. Sit volutpat dissentiet ex, eos soluta.</Title>
            </section>
            <section className='text--mb--m'> 
                <div className='text--mb--s'>
                <Title headingStyle='bolder--800' headingSize='heading--h5' > Subtitle</Title>
                </div>
            <Title headingStyle ='subtitle' headingSize='heading--small'>Lorem ipsum dolor sit amet, an vel iudico doctus comprehem, has an blandit moder atius liberavisse, vim dicta aeque choro ut. Debet simulus principes qui ut, melius necessi tat ibus an. Te posse idque disse ntiunt est, ne nec vidisse deleniti hendrerit. Legere grae per ut, ad eam altera electram. Sit volutpat dissentiet ex, eos soluta.</Title>
            </section>

            
            
        </div>
    )
}

export default Headings
