import React from 'react'
import Descreption from '../components/DescreptionSection/Descreption.component'

const Photography = () => {
    return (
        <div className='container' style={{marginTop: '100px'}}>
            <div className='container'>
                <div style={{marginBottom: '50px'}}>

            <Descreption  title='Photography' paragraphe='I always enjoy exploring different cities and nature areas, caring my camera and lenses in my backpack, whenever I get the opportunity. I love capturing the landscapes and the city sights, that I come across, as well as the local people while living their life. 

Here are some experts of photos, I took in different cities in both Tunisia and Denmark.'/>
                </div>
                
            <img style={{marginBottom: '2rem'}} src='https://i.ibb.co/J2KsGNj/MG-8682.jpg' alt=''/>
            <img style={{marginBottom: '50px'}} src='https://i.ibb.co/nwQVFKc/MG-8777.jpg' alt=''/>
            <img style={{marginBottom: '50px'}} src='https://i.ibb.co/f2XfL2T/MG-8649.jpg' alt=''/>
            <img style={{marginBottom: '50px'}} src='https://i.ibb.co/LS8tN1B/MG-8773.jpg' alt=''/>
            <img style={{marginBottom: '50px'}} src='https://i.ibb.co/87d70mB/10626323-10203825137726634-4225218816575675830-o.jpg' alt=''/>
            <img style={{marginBottom: '50px'}} src='https://i.ibb.co/YBkxWW0/MG-2326-2123.jpg' alt=''/>
            <img style={{marginBottom: '50px'}} src='https://i.ibb.co/dQ0n3b0/MG-606801.png' alt=''/>
            <img style={{marginBottom: '50px'}} src='https://i.ibb.co/6rczTdZ/IMG-34931.png' alt=''/>
            <img style={{marginBottom: '50px'}} src='https://i.ibb.co/5nsp04Y/3.png' alt=''/>
            <img style={{marginBottom: '50px'}} src='https://i.ibb.co/b6yr5Mn/1913499-10202469882846109-1951488090-o.jpg' alt=''/>
            <img style={{marginBottom: '50px'}} src='https://i.ibb.co/LJwsT6b/MG-0255-3.jpg' alt=''/>
            <img style={{marginBottom: '50px'}} src='https://i.ibb.co/2Wcxh1v/IMG-0849.jpg' alt=''/>
            <img style={{marginBottom: '50px'}} src='' alt=''/>
             </div>
        </div>
    )
}

export default Photography
