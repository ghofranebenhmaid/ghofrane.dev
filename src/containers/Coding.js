import React from 'react';
import Descreption from '../components/DescreptionSection/Descreption.component';
import Repositories from '../components/Repositories/Repositories.component';

const Coding = () => {
   return (
      <div className='container'>
         <div className='container' style={{marginTop: '100px'}}>
            <Descreption
               title='Websites'
               paragraphe='Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book'
            />
         </div>

         <Repositories />
      </div>
   );
};

export default Coding;
