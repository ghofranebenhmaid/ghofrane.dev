import React from 'react';
import Descreption from '../DescreptionSection/Descreption.component';
import ghofImg from '../../assets/images/ghofranebh.png';
import Button from '../Button/Button';
import { Link } from 'react-router-dom';

const Profile = () => {
   return (
      <div className='profile container'>
         <div className='profile_img'>
            <img src={ghofImg} alt='ghofrane image' />
         </div>
         <div className='profile_desc'>
            <Descreption
               title='Ghofrane Ben Hmaid'
               paragraphe='I’m a skilled and diligent frontend developer with a passion for React and a flair for user interface design. I have
               experience with different programming languages and technologies including HTML, CSS, JavaScript, Node.js, Express.js,
               React, MySQL, Knex.js and Git as well as WordPress. I’m also very skilled in using Adobe design programs, such as
               Photoshop, Illustrator, InDesign, Lightroom, Premier Pro, After Effects and 3Ds max.'
            />
            <Descreption
               
                   paragraphe='
                I’m educated in IT & Multimedia, have completed the HackYourFuture web-development education program, and have achieved
               a lot of learning-by-doing through the years.'
            />
            <Descreption
               paragraphe='I have a broad skillset beside web-development including graphic design, user interface design, typography and photo and
               video editing. In my spare time I also enjoy photography and exploring the city and the nature.'
               />
               <div style={{marginTop: '20px'}}>

            <Link to='/about' className='white-cursor translate-cursor'>
               <Button buttonSize='btn--medium'>See Resume</Button>
            </Link>
               </div >
         </div>
      </div>
   );
};

export default Profile;
