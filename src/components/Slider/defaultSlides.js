export default [
    {
        title: 'First slide!',
        subtitle: ' 🅰️ Laboramus ullamcorper sit ad. Lusto urbanitas aliq am ea cum, eu propriae accusata quo, videm inter esset eum te. Moremis timeam id cum dos.',
    },
    {
        title: "Second Slide",
        subtitle: " 🌟 Laboramus ullamcorper sit ad. Lusto urbanitas aliq am ea cum, eu propriae accusata quo, videm inter esset eum te. Moremis timeam id cum dos.",
    },
    {
        title: "Third Slide",
        subtitle: " ✨ Laboramus ullamcorper sit ad. Lusto urbanitas aliq am ea cum, eu propriae accusata quo, videm inter esset eum te. Moremis timeam id cum dos.",
    },
]